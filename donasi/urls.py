from django.urls import path
from . import views

app_name = 'donasi'

urlpatterns = [
    path('', views.donasi, name='donasi'),
    path('formdonasi', views.form_donasi, name='form_donasi'),
    path('donasifiltered', views.donation, name='donation'),
]