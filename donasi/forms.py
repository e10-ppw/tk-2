from django import forms
from donasi.models import fitur_donasi, searchForm

class DonasiForm(forms.ModelForm):
        nama_donasi = forms.CharField(label='Nama Donasi :', max_length=50,
                                        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ex: Donasi RS Harapan Ibu'}), required=True)

        nama_pemberi = forms.CharField(label='Author :', max_length=50,
                                    widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ex: Nama anda/institusi/organisasi/dll'}), required=True)

        url = forms.CharField(label='URL Donasi :', max_length=50,
                                        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'masukkan URL'}), required=True)

        class Meta:
                model = fitur_donasi
                fields = "__all__" #ambil semua yang terdapat pada model

class SearchForm(forms.ModelForm):
    search = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control form-control-sm','id':'txtSearch', 'placeholder': 'Cari nama donasi'}),
    )
    class Meta:
        model = searchForm
        fields = "__all__" 