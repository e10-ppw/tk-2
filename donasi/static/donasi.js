$(document).ready(function () {
    $("#txtSearch").keyup( function () {
        var q = $('#txtSearch').val();
        $.ajax({
            url: '/donasi/donasifiltered?q=' + q,
            success: function (result) {
                var views = $('#row-card');
                views.html('');
                for (i = 0; i < result.length; i++) {
                    var title = result[i].fields.nama_donasi;
                    var donatur = result[i].fields.nama_pemberi;
                    var link = result[i].fields.url;
                    console.log(title);

                    card = `<div class="col-md-4 mb-4 area">
                    <div class="card cardku" id="donate" style="max-width: 22rem; text-align: center; margin: 0 auto; float: none; margin-bottom: 10px;">
                        <!-- card header -->
                        <div class="card-header" style="font-weight: bold; background-color: #FFEFCA;">
                            <h5 class="card-title" style="color: #006876; font-weight: bold;"> ${title} </h5>
                        </div>
                        <div class="card-body">
                            <p style="font-weight: bold;">oleh ${donatur} </p>
                            <a href="${link}" class="btn btn-primary btnijo">Lihat lebih lanjut</a>
                        </div>
                    </div>`
                    views.append(card);
                }
            }
        });

    });

    $('#activity-btn').click( function () {
        $('#activity-body').toggleClass('hidden');
    })

    $("#form-donasi").submit( function(e) {
        e.preventDefault();
        console.log(this);
        $.ajax({
            url: location.origin + '/donasi/formdonasi',
            type: "POST",
            data: $(this).serialize(),
            success: function (result) {
                // $('input').val("");
                // $(this).closest('form').find("input[type=text]").val("");
                $("form").trigger("reset");
                var views = $('#row-card');
                var title = result.nama_donasi[0];
                var donatur = result.nama_pemberi[0];
                var link = result.url[0];
                console.log(title);

                card = `<div class="col-md-4 mb-4 area">
                <div class="card cardku" id="donate" style="max-width: 22rem; text-align: center; margin: 0 auto; float: none; margin-bottom: 10px;">
                    <!-- card header -->
                    <div class="card-header" style="font-weight: bold; background-color: #FFEFCA;">
                        <h5 class="card-title" style="color: #006876; font-weight: bold;"> ${title} </h5>
                    </div>
                    <div class="card-body">
                        <p style="font-weight: bold;">oleh ${donatur} </p>
                        <a href="${link}" class="btn btn-primary btnijo">Lihat lebih lanjut</a>
                    </div>
                </div>`
                views.append(card);
            }
            
        })
        console.log($(this).serialize());
    })



    $("#tambahdonasi").hover(function() {
        $(this).css("background-color", "rgb(0, 104, 118, 0.1)");
        }, function(){
        $(this).css("background-color", "rgb(0, 104, 118, 0.1)");
    });

});
