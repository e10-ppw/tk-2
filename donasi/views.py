from django.shortcuts import render, redirect
from django import forms
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from donasi.forms import DonasiForm
from donasi.models import fitur_donasi, searchForm
from django.db.models import Q
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers

# Create your views here.
def form_donasi(request):
    form = DonasiForm(request.POST or None)
    donate_context = {
        'form' : form,
    }
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return JsonResponse(dict(request.POST.lists()))
    return render(request, 'donasi.html', donate_context)

def donasi(request):
    data = fitur_donasi.objects.all()
    form = DonasiForm(request.POST or None)
    response = {
        'data': data,
        'form' : form,
    }
    return render(request, 'donasi.html', response)

def donation(request):
    text = request.GET.get('q', '')
    pesan = fitur_donasi.objects.filter(nama_donasi__contains=text).order_by('-nama_donasi')
    JSONpesan = serializers.serialize('json', pesan)
    JSONpesan = json.loads(JSONpesan)
    return JsonResponse(JSONpesan, json_dumps_params={'indent': 2}, safe=False)