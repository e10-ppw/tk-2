# Generated by Django 3.1.3 on 2020-11-14 22:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='KritikSaran',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('kritik_saran', models.TextField(max_length=1000)),
            ],
        ),
    ]
