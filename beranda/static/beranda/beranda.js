$(document).ready(function () {

    $.ajax({
        url: "/data-covid/",
        success: function(data){
            var positif = data.Countries[77].TotalConfirmed
            var sembuh = data.Countries[77].TotalRecovered
            var meninggal = data.Countries[77].TotalDeaths
            var tanggal = data.Date
            
            document.getElementById('confirmed').innerHTML = positif
            document.getElementById('recovered').innerHTML = sembuh
            document.getElementById('death').innerHTML = meninggal
            document.getElementById('tanggal').innerHTML = 'Update Terakhir: ' + tanggal
        }
    })

    $("form").submit(function(){
        confirm("Tanggapan Anda akan kami simpan dan Anda tidak dapat mengubahnya lagi, apakah Anda yakin?")
    })

    $(window).scroll(function(){
        if($(this).scrollTop() > 50){
            $("#toTop").fadeIn();
        } else {
            $("#toTop").fadeOut();
        }
    })

    $("#toTop").click(function(){
        $('html, body').animate({scrollTop : 0}, 800);
    })
})