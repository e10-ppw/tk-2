$(document).ready(function () {
    $("#keyword").on("keyup", function(e){
        if (e.which === 13){
            cari();
        }
    })

    $("#search").click(function(){
        cari();
    });

    function cari(){
        var keyword = $("#keyword").val();
        $.ajax({
            url: '/search-kritik?q=' + keyword,
            success: function(hasil){
                var result = $("#bodyTable");
                result.empty()
                for (x = 0; x < hasil.length; x++){
                    var name = hasil[x].fields.nama;
                    var kritik = hasil[x].fields.kritik_saran;
                    result.append("<tr><td>" + name + "</td><td>" + kritik + "</td></tr>");
                }
            }
        })
    }

    $(window).scroll(function(){
        if($(this).scrollTop() > 50){
            $("#toTop").fadeIn();
        } else {
            $("#toTop").fadeOut();
        }
    })

    $("#toTop").click(function(){
        $('html, body').animate({scrollTop : 0}, 800);
    })
})