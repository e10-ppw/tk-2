from django.urls import path
from . import views

app_name = 'beranda'

urlpatterns = [
    path('', views.home, name='home'),
    path('daftar-kritikan/', views.daftar, name='daftar'),
    path('data-covid/', views.data_covid, name='data'),
    path('search-kritik', views.search_kritik, name='search'),
]