import json, requests
from django.http.response import JsonResponse
from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import FormKritik
from .models import KritikSaran
from django.core import serializers
from django.http import HttpResponse
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def home(request):
    form = FormKritik(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        messages.success(request, 'Tanggapan Anda telah kami simpan')
        return redirect('beranda:home')
        
    context = {
        'input_form' : form
    }
    return render(request, 'beranda/home.html', context)

def daftar(request):
    kritikans = KritikSaran.objects.all()
    context = {
        'daftar_kritik' : kritikans
    }
    
    return render(request, 'beranda/kritik-saran.html', context)

def data_covid(request):
    url = 'https://api.covid19api.com/summary'
    r = requests.get(url=url, verify=False)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)

def search_kritik(request):
    keyword = request.GET.get('q', '')
    kritikan = KritikSaran.objects.filter(nama__contains=keyword).order_by('-nama')
    JSONkritik = serializers.serialize('json', kritikan)
    JSONkritik = json.loads(JSONkritik)
    return JsonResponse(JSONkritik, json_dumps_params={'indent':2}, safe=False)