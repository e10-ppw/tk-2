from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from .models import RumahSakit

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

content = {}

def index(request):
    content['rumah_sakit'] = []
    list_rumahsakit = RumahSakit.objects.all()

    pages = Paginator(list_rumahsakit, 3)
   
    for rs in list_rumahsakit:
        content['rumah_sakit'].append({
            'name':rs.name,
            'province':rs.province,
            'address':rs.address,
            'telp':rs.telp,
           })

    page_number = request.GET.get('page')
    page_obj = pages.get_page(page_number)

    return render(request, 'rs/rs-rujukan.html',  { 'rumah_sakit' : list_rumahsakit,
        'page_obj': page_obj})

def search(request):
    search_words = request.GET.get('search')

    if search_words:
        results = RumahSakit.objects.filter(Q(province__icontains=search_words) | Q(address__icontains=search_words))
    else:
        results = RumahSakit.objects.filter(status="Published")

    pages = Paginator(results, 3)
    page_number = request.GET.get('page')
    page_obj = pages.get_page(page_number)

    content = {
        'page_obj' : page_obj,
        'search_words': search_words
    }

    return render(request, 'rs/rs-rujukan.html', content)