from django.db import models
from _datetime import datetime

class Pesan(models.Model): 
    nama = models.CharField(max_length=30, blank= False)
    tujuan = models.CharField(max_length=30, blank= False)
    kota = models.CharField(max_length=20, blank= False, default = None)
    pesan = models.CharField(max_length=100, blank= False)

class SearchBar(models.Model):
    search = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now(), blank=True)





