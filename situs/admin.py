from django.contrib import admin
from .models import Pesan, SearchBar


admin.site.register(Pesan)
admin.site.register(SearchBar)