from django.test import TestCase, Client
from django.urls import resolve
from .views import situs, pesan, fpesan
from .models import Pesan
from .forms import Input_Form

# Create your tests here.
class Lab6UnitTest(TestCase):
	def test_url_exist_satu(self):
		response = Client().get('/situs/savepesan/')
		self.assertEqual(response.status_code,302)

	def test_url_exist_dua(self):
		response = Client().get('/situs/')
		self.assertEqual(response.status_code,200)

	def test_url_fungsi_pesan(self): 
		found = resolve('/situs/savepesan/')
		self.assertEqual(found.func,pesan)

	def test_url_fungsi_situs(self): 
		found = resolve('/situs/')
		self.assertEqual(found.func,situs)

	def test_on_page(self) :
		response = Client().get('/situs/')
		content = response.content.decode('utf8')
		self.assertIn("<h1><b>DAFTAR</b></h1>", content)

	def test_html(self) :
		response = Client().get('/situs/')
		self.assertTemplateUsed(response, 'situs.html')

	def test_create_models(self):
		Pesan.objects.create(nama ='efrin', tujuan = 'Mama', kota = 'Tangsel', pesan = 'apa kabar Ma')
		counting_new_model = Pesan.objects.all().count()
		self.assertEqual(counting_new_model,1)

	def test_form_is_blank(self): 
		form = Input_Form(data = {})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors, {
			'nama':['This field is required.'], 
			'tujuan':['This field is required.'],
			'kota':['This field is required.'],
			'pesan':['This field is required.'],
		})

	def test_form_is_valid(self):
		form = Input_Form( data = {
			'nama': "efrin", 
			'tujuan' : "mamah", 
			'kota' : "semarang", 
			'pesan' : "hai mamah",
		})
		self.assertTrue(form.is_valid())	
		pesans = form.save()
		self.assertEqual(pesans.nama, "efrin")
		self.assertEqual(pesans.tujuan, "mamah")
		self.assertEqual(pesans.kota, "semarang")
		self.assertEqual(pesans.pesan, "hai mamah")

	def test_html_text_after_login(self) :
		if Client().login(username='coba', password='coba123'):
			response = Client().get('/situs/')
			html_response = response.content.decode('utf8')            
			self.assertIn('<h5>&#128521 Hello coba &#128521</h5>', html_response)

	#JSON
	def test_url_exist_tiga(self):
		response = Client().get('/situs/pesanfiltered/')
		self.assertEqual(response.status_code,200)
		
	def test_function_exist(self):
		found = resolve('/situs/pesanfiltered/')
		self.assertEqual(found.func,fpesan)

	

	

	


	

	


