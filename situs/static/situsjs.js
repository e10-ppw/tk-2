$(document).ready(function () {
    $(".accordion").on("click", function() {
        var panelTarget = $(".panel");
        if (panelTarget.attr("class").includes("active")) { //sudah aktif
            panelTarget.removeClass("active");
        } else { //belum aktif
            panelTarget.addClass("active");
        }
    });
    $(".accordion").hover(function() {
        $(this).css("background-color", "#555");
        }, function(){
        $(this).css("background-color", "#F4C430");
    });
    $("form").submit(function(){
        alert("Pesan berhasil terkirim!");
    });
    $('#search_input').on("keyup", function () {
        var q = $('#search_input').val().toLowerCase();
        $.ajax({
            url: '/situs/pesanfiltered/',
            success: function (result) {
                var tampilan = $('#pesans');
                tampilan.html('');
                for (i=0; i< result.length; i++) {
                    var ntujuan = result[i].fields.tujuan.toLowerCase();
                    if (ntujuan.includes(q)) {
                        appended = '<div class = "row" id = "rcard"> <div class="card text-white bg-info mb-3" id = "card">' +
                        '<div class="card-header" id = "header" > PESAN </div> <div class="card-body"> <h5 class="card-title" ' + 
                        'id = "title">Dari ' + result[i].fields.nama + ' untuk ' + result[i].fields.tujuan + ' di ' + result[i].fields.kota + 
                        '</h5> <p class="card-text" id = "message"> ' + result[i].fields.pesan + '</p> </div> </div> </div>';
                        tampilan.append(appended);
                    }
                }
            }
        });
    });
});