from django.shortcuts import render
from situs.forms import Input_Form, SearchForm
from situs.models import Pesan, SearchBar
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.contrib.sites import requests

def situs(request):
    pesans = Pesan.objects.all()
    content = {'input_form' : Input_Form(), 'pesans': pesans}
    return render(request, 'situs.html',content)

def pesan(request):
    form = Input_Form(request.POST or None)
    if (request.method == 'POST') :
        if (form.is_valid()) :
            form.save()
            return HttpResponseRedirect('/situs')
    else :
        return HttpResponseRedirect('/')

def fpesan(request):
    pesan = Pesan.objects.all()
    JSONpesan = serializers.serialize('json', pesan)
    return HttpResponse(JSONpesan, content_type="application/json")
    