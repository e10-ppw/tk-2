
from .models import Pesan
from django import forms
from situs.models import SearchBar

class Input_Form(forms.ModelForm):
    class Meta:
        model = Pesan
        fields = "__all__" #ambil semua yang terdapat pada model
        
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Maks 30 karakter'
    }

    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'Maks 30 karakter'
    }

    input_attrs3 = {
        'type' : 'text',
        'placeholder' : 'Maks 20 karakter'
    }

    input_attrs4 = {
        'type' : 'text',
        'placeholder' : 'Maks 100 karakter'
    }

    nama = forms.CharField(label = 'Nama Anda', required=True,
        max_length=30, widget=forms.TextInput(attrs=input_attrs1))
    tujuan = forms.CharField(label = 'Nama Penerima', required=True,
        max_length=30, widget=forms.TextInput(attrs=input_attrs2))
    kota = forms.CharField(label = 'Kota Penerima', required=True,
        max_length=20, widget=forms.TextInput(attrs=input_attrs3))
    pesan = forms.CharField(label = 'Pesan', required=True,
        max_length=100, widget=forms.TextInput(attrs=input_attrs4))

class SearchForm(forms.ModelForm):
    search = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control form-control-sm','id':'search_input', 'placeholder': 'Masukkan keyword nama'}),
    )
    class Meta:
        model = SearchBar
        fields = "__all__" 


    



    


